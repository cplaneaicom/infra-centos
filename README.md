#  Devops area - Centos 7 and 8

This is part of the devops docker tools.

It provides docker file that is setting up Centos7/8 yum proxy and nodejs proxy 
## Prerequisites

* docker engine ce ( not the default docker of the linux distribution; look at docker site for install instructions), https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-from-a-package
* unix based OS. Tested on ubuntu 16.04 and Centos 7 ( might work on windows too )


## Acknowledgments

* Devops tools is part of **Devops area**
